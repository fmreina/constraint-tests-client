## CONSTRAINT TESTS CLIENT
The [Constraint-Tests-Client](https://bitbucket.org/fmreina/constraint-tests-client/src/master/) is part of a project to evaluate the impact of the use of **Integrity Constraints** (IC) on a **Graph Database** (DB). It contains the time evaluation tests of the four new constraints added to the OrientDB database as an extention of the supported constraints.

The new ICs added to the OrientDB are:

- Conditional
- In/Out
- Required Edge
- Cardinality

The tests are ran for the different constraint types, specified above, and for three scenario variants: **Original, Modified, Application**. 

The first one uses the *Original* OrientDB server, without modifications. The second uses the *Modified* version of the server, with the extended support for ICs. The last variant uses the original server and executes the IC validation on the client-side before sending the transactions to the server.

This project works in combination with other two project named *Orient-Driver* and *OrientDB Server*. The links for these projects are listed below.

- [Orient-Driver](https://bitbucket.org/fmreina/orient-driver/src/master/): Initiates a server of the OrientDB and the Constraint-Tests-Client to run the tests
- OrientDB server:
	- [Modified version](https://bitbucket.org/fmreina/orientdb/src/master/): Extended with the support for the new proposed constraints
	- [Original version](https://bitbucket.org/fmreina/orientdb/src/original/): Original code, without the support of the new constraints


**Obs.:** This project has as dependencies the two versions to the OrientDB server. It is necessary to build a different executable jar file for each of the two versions (modified and original). For that use the following commands:

- ``$ mvn package`` : to run with the original version
 - ``$ mvn -Pcustom package`` : to run with the modified version

