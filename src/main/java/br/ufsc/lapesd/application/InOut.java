package br.ufsc.lapesd.application;

import static br.ufsc.lapesd.database.TestType.INOUT;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import javax.management.RuntimeErrorException;

import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.OVertexDocument;

public class InOut extends AppTest {

	public InOut() {
		super();
		DATABASE_NAME = "InOut";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Owns");

		createPreConditions();
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", INTEGER);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
		if (edge == null)
			edge = dbSession.createEdgeClass(name);
	}

	@Override
	public void run(int execution) {
		long startTime;
		long endTime;
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT)
			createEntities(NUM_INSERTS, MOD);
		endTime = System.nanoTime();

		if (DELETE_ALL_RECORDS) {
			deleteAllEntities("Person");
		}

		if (DROP_DB)
			dropDatabase();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	private void createPreConditions() {
		//System.out.println(">>>>>>> Creating pre-conditions");
		Map<String, String> propertiesPerson = new HashMap<>();
		Map<String, String> propertiesCompany = new HashMap<>();
		Map<String, String> propertiesCity = new HashMap<>();
		String typePerson = "Person";
		String typeCompany = "Company";
		String typeCity = "City";

		for (int i = 0; i < NUM_INSERTS; i++) {
			propertiesPerson.put("id", "" + i);
			propertiesPerson.put("name", "Person_" + i);

			propertiesCompany.put("id", "" + i);
			propertiesCompany.put("name", "Company_" + i);

			propertiesCity.put("id", "" + i);
			propertiesCity.put("name", "City_" + i);

			try {
				OVertex city = createVertex(dbSession, typeCity, propertiesCity);
				OVertex person = createVertex(dbSession, typePerson, propertiesPerson);
				OVertex company = createVertex(dbSession, typeCompany, propertiesCompany);
			} catch (Exception e) {
//				System.out.println("Error creating vertex.\n\t" + e.getMessage());
			} finally {
				propertiesPerson.clear();
			}
		}
	}

	@Override
	protected void createEntities(int numInserts, int mod) {
		String typePerson = "Person";
		String typeCompany = "Company";
		String typeCity = "City";
		String ownsType = "owns";

		for (int i = 0; i < NUM_INSERTS; i++) {
			OVertex person = selectFrom(dbSession, typePerson, "name", "Person_" + i);
			OVertex company = selectFrom(dbSession, typeCompany, "name", "Company_" + i);
			OVertex city = selectFrom(dbSession, typeCity, "name", "City_" + i);

			try {
				if (i % MOD == 0) {
					validate(person, ownsType, company);
					// valid insert
					createEdge(person, company, ownsType);
				} else {
					validate(city, ownsType, company);
					// invalid insert
					createEdge(person, city, ownsType);
				}
				countInserts++;
				// System.out.println("Edge was created.");
			} catch (Exception e) {
				countRejects++;
				// System.out.println("Edge was not created: \n\t" + e.getMessage());
			}
		}
	}

	private boolean validate(OVertex outNode, String ownsType, OVertex inNode) {
		// CREATE CONSTRAINT inout ON Owns IN_OUT_EDGE FROM Person TO Company;
		if (!ownsType.equalsIgnoreCase("Owns")) {
			throw new RuntimeErrorException(new Error(">>>>> Validation failed! Edge type is wrong"));
		}

		OVertexDocument outVD = (OVertexDocument) outNode;
		if (!outVD.getClassName().equalsIgnoreCase("Person")) {
			throw new RuntimeErrorException(new Error(">>>>> Validation failed! Out node type is wrong"));
		}

		OVertexDocument inVD = (OVertexDocument) inNode;
		if (!inVD.getClassName().equalsIgnoreCase("Company")) {
			throw new RuntimeErrorException(new Error(">>>>> Validation failed! In node type is wrong"));
		}

		// System.out.println("Data is valid!");
		return true;
	}

	@Override
	protected String getTestName() {
		return INOUT.name() + "_APP";
	}
}
