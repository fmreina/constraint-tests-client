package br.ufsc.lapesd.application;

import static br.ufsc.lapesd.database.TestType.CONDITIONAL;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import javax.management.RuntimeErrorException;

import com.orientechnologies.orient.core.metadata.schema.OClass;

public class Conditional extends AppTest {

	public Conditional() {
		super();
		DATABASE_NAME = "Conditional";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	@Override
	protected void createSchema() {
		createNode("Person");
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", INTEGER);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);

		if (node.getProperty("attrib1") == null)
			node.createProperty("attrib1", INTEGER);

		if (node.getProperty("attrib2") == null)
			node.createProperty("attrib2", INTEGER);
	}

	@Override
	public void run(int execution) {
		long startTime;
		long endTime;
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT)
			createEntities(NUM_INSERTS, MOD);
		endTime = System.nanoTime();

		if (DELETE_ALL_RECORDS)
			deleteAllEntities("Person");

		if (DROP_DB)
			dropDatabase();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void createEntities(int numInserts, int mod) {
		Map<String, String> properties = new HashMap<>();
		String type = "Person";

		for (int i = 0; i < numInserts; i++) {
			properties.put("id", "" + i);
			properties.put("name", "Person_" + i);
			if (i % mod == 0) {
				// valid insert
				properties.put("attrib1", "1");
				properties.put("attrib2", "2");
			} else {
				// invalid insert
				properties.put("attrib1", "3");
				properties.put("attrib2", "4");
			}
			try {
				validate(properties);
				
				createVertex(dbSession, type, properties);
				countInserts++;
				//System.out.println(">>>>> Vertex '" + properties.get("name") + "' was created.");
			} catch (Exception e) {
				countRejects++;
				//System.out.println(">>>>> Vertex '" + properties.get("name") + "'was not created: \n\t" + e.getMessage());
				//System.out.println(e.getMessage());
			} finally {
				properties.clear();
			}
		}
	}

	private boolean validate(Map<String, String> properties) {
		// IF attrib2 < 3 THEN attrib1 < 2 ELSE attrib1 > 4
		//System.out.println(">>>>> Data is being validated..");
		if (Integer.parseInt(properties.get("attrib2")) < 3) {
			if (Integer.parseInt(properties.get("attrib1")) < 2)
				return true;
		} else if (Integer.parseInt(properties.get("attrib1")) > 4)
			return true;

		throw new RuntimeErrorException(new Error(">>>>> Data validation failed!"));
	}

	@Override
	protected String getTestName() {
		return CONDITIONAL.name() + "_APP";
	}
}
