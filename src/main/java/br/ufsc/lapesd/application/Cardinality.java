package br.ufsc.lapesd.application;

import static br.ufsc.lapesd.database.TestType.CARDINALITY;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.Map;

import javax.management.RuntimeErrorException;

import com.orientechnologies.orient.core.command.script.OCommandScript;
import com.orientechnologies.orient.core.db.record.OIdentifiable;
import com.orientechnologies.orient.core.db.record.ridbag.ORidBag;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OBasicLegacyResultSet;

public class Cardinality extends AppTest {

	public Cardinality() {
		super();
		DATABASE_NAME = "Cardinality";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Owns");
		createEdge("Lives");
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
	    if (node == null)
	      node = dbSession.createVertexClass(name);
	    
	    if (node.getProperty("id") == null)
	    	node.createProperty("id", INTEGER);

	    if (node.getProperty("name") == null)
	      node.createProperty("name", STRING);

	    if (node.getProperty("attrib1") == null)
	      node.createProperty("attrib1", INTEGER);

	    if (node.getProperty("attrib2") == null)
		      node.createProperty("attrib2", INTEGER);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
	    if (edge == null)
	      edge = dbSession.createEdgeClass(name);
	}
	
	@Override
	public void run(int execution) {
		long startTime;
		long endTime;
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT)
			createEntities(NUM_INSERTS, MOD);
		endTime = System.nanoTime();

//		if (DELETE_ALL_RECORDS){
//			System.out.println("Deleting records "+ new Date());
//			deleteAllEntities("Person");
//		}

		if (DROP_DB)
			dropDatabase();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void createEntities(int numInserts, int mod) {
//		System.out.println(">>>>>> CREATE ENTITIES...");
		for (int i = 0; i < NUM_INSERTS; i++) {
			try {
				batchCreateValid(i);
				countInserts++;
			} catch (Exception e) {
//				System.out.println("Action rejected!\n" + e.getCause() + "\n" + e.getMessage());
				countRejects++;
			}
		}
		for (int i = 0; i < NUM_INSERTS; i++) {
			try {
				createNewEdge(i);
			} catch (Exception e) {
//				System.out.println("Action rejected!\n" + e.getCause() + "\n" + e.getMessage());
			}
		}
	}
	
	private void createNewEdge(int i) {
		OVertex personNode = selectFrom(dbSession, "Person", "name", "Person_" + i);
		OVertex cityNode = selectFrom(dbSession, "City", "name", "City_" + i);
		OVertex companyNode = selectFrom(dbSession, "Company", "name", "Company_4." + i);

//		if (i % 2 == 0){		
			if(validateEdgeOwns(personNode, cityNode, "Lives")){
				createEdge(personNode, cityNode, "Lives");
//				System.out.println(">>>>> Edge created.. " + i);
			}
//		} else 
//			if(validateEdgeOwns(personNode, companyNode, "Owns"))
//				createEdge(personNode, companyNode, "Owns");
	}

	private boolean validateEdgeOwns(OVertex outNode, OVertex inNode, String edgeType) {
		String lLimitString = "N";
		int lLimitInt = 3;
		// String rLimitString = "N";
		int rLimitInt = 3;
		ORidBag out = outNode.getProperty("out_Owns");
		ORidBag in = inNode.getProperty("in_Owns");
		
		int rCounter = 0;
		if(((ODocument)outNode).getClassName().equalsIgnoreCase("Person") && 
				((ODocument)inNode).getClassName().equalsIgnoreCase("Company") && 
				edgeType.equalsIgnoreCase("Owns")){
			if (out != null && !"N".equalsIgnoreCase(String.valueOf(rLimitInt))) {
				for (OIdentifiable edge : out) {
					ODocument edgeDoc = (ODocument) edge;
					if ("Owns".equalsIgnoreCase(edgeDoc.getClassName()))
						rCounter++;
				}
				
				if (rCounter >= rLimitInt)
					// return false;
					throw new RuntimeErrorException(new Error(String.format(">>>>> Validation failed! Cannnot add a new edge.")));
			}
			
			int lCounter = 0;
			if (in != null && !"N".equalsIgnoreCase(lLimitString)) {
				for (OIdentifiable edge : in) {
					ODocument edgeDoc = (ODocument) edge;
					if ("Owns".equalsIgnoreCase(edgeDoc.getClassName()))
						lCounter++;
				}
				
				if (lCounter >= lLimitInt)
					// return false;
					throw new RuntimeErrorException(new Error(String.format(">>>>> Validation failed! Cannot add a new edge.")));
			}
		}
		return true;
	}

	private void batchCreateValid(int execNumber) {
		String cmd = "begin\n";
		cmd += "let person = create vertex Person set id= " + execNumber + ", name = 'Person_" + execNumber + "'\n";
		cmd += "let company1 = create vertex Company set id= " + (execNumber + 1) + ", name = 'Company_1." + execNumber + "'\n";
		cmd += "let company2 = create vertex Company set id= " + (execNumber + 2) + ", name = 'Company_2." + execNumber + "'\n";
		cmd += "let company3 = create vertex Company set id= " + (execNumber + 3) + ", name = 'Company_3." + execNumber + "'\n";
		cmd += "let company4 = create vertex Company set id= " + (execNumber + 4) + ", name = 'Company_4." + execNumber + "'\n";
		cmd += "let owns1 = create edge Owns from $person to $company1\n";
		cmd += "let owns2 = create edge Owns from $person to $company2\n";
		cmd += "let owns3 = create edge Owns from $person to $company3\n";
		
		// cmd += "let owns4 = create edge Owns from $person to $company4\n";

		cmd += "let city = create vertex City set id= " + execNumber + ", name = 'City_" + execNumber + "'\n";
		// cmd += "let owns = create edge Lives_in from $person to $city\n";

		cmd += "commit retry 100\n";
		cmd += "return $owns1";

		OBasicLegacyResultSet edge = dbSession.command(new OCommandScript("sql", cmd)).execute();
		// edge.forEach(e -> System.out.println(e.toString()));
	}

	private boolean validate(Map<String, String> properties) {
//		CREATE CONSTRAINT card ON Person CARDINALITY Owns N .. 3 TO Company;
//		create vertex person set nome = "Zach";
//		create vertex Company set nome="Jeep";
//		create edge owns from #17:0 to #21:0

		throw new RuntimeErrorException(new Error(">>>>> Data validation failed!"));
	}

	@Override
	protected String getTestName() {
		return CARDINALITY.name() + "_APP";
	}
}
