package br.ufsc.lapesd.application;

import static br.ufsc.lapesd.database.TestType.REQUIREDEDGE;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import javax.management.RuntimeErrorException;

import com.orientechnologies.orient.core.command.script.OCommandScript;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.OVertexDocument;
import com.orientechnologies.orient.core.sql.query.OBasicLegacyResultSet;

public class RequiredEdge extends AppTest {

	public RequiredEdge() {
		super();
		DATABASE_NAME = "RequiredEdge";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Owns");
		createEdge("Lives");
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
	    if (node == null)
	      node = dbSession.createVertexClass(name);
	    
	    if (node.getProperty("id") == null)
	    	node.createProperty("id", INTEGER);

	    if (node.getProperty("name") == null)
	      node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
	    if (edge == null)
	      edge = dbSession.createEdgeClass(name);
	    
	    if(edge.getProperty("name") == null)
	    	edge.createProperty("name", STRING);
	}
	
	@Override
	public void run(int execution) {
		long startTime;
		long endTime;
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT)
			createEntities(NUM_INSERTS, MOD);
		endTime = System.nanoTime();

		// if (DELETE_ALL_RECORDS){
		// deleteAllEntities("Person");
		// deleteAllEntities("Company");
		// }

		if (DROP_DB)
			dropDatabase();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void createEntities(int numInserts, int mod) {
//		System.out.println(">>>>>> CREATE ENTITIES...");
		for (int i = 0; i < NUM_INSERTS; i++) {
			try {
					batchCreateValid(i);
				countInserts++;
			} catch (Exception e) {
//				System.out.println("Action rejected!\n" + e.getCause() + "\n" + e.getMessage());
				countRejects++;
			}
		}
		for (int i = 0; i < NUM_INSERTS; i++) {
			try {
				deleteEdgeTest("Lives", "Lives",i);
			} catch (Exception e) {
//				System.out.println("Action rejected!\n" + e.getCause() + "\n" + e.getMessage());
			}
		}
	}

	private void deleteEdgeTest(String className, String edgeName, int execNumber) throws Exception {
//		OEdge edge = selectEdgeFrom(dbSession, edgeName, "name", "Owns_" + execNumber);
		OEdge edge = selectEdgeFrom(dbSession, edgeName, "name", edgeName + "_" + execNumber);
		OVertex outNode = edge.getVertex(ODirection.OUT);
		OVertex inNode = edge.getVertex(ODirection.IN);
		if (validateDelete(edge, outNode, inNode))
			deleteRecord(dbSession, edge);
//		System.out.println(">>>>> Edge was deleted: " + edgeName);
	}

	private boolean validateDelete(OEdge edge, OVertex outNode, OVertex inNode) throws RuntimeErrorException {
		OVertexDocument outVD = (OVertexDocument) outNode;
		OVertexDocument inVD = (OVertexDocument) inNode;
		if (outVD.getClassName().equalsIgnoreCase("Person") && inVD.getClassName().equalsIgnoreCase("Company")) {
			throw new RuntimeErrorException(new Error(String.format(">>>>> Validation failed! Not allowed to delete a required edge between %s and %s.", outVD.getClassName(), inVD.getClassName())));
		}
		return true;
	}

	private void batchCreateValid(int execNumber) {
		String cmd = "begin\n";
		cmd += "let person = create vertex Person set id= " + execNumber + ", name = 'Person_" + execNumber + "'\n";
		cmd += "let company = create vertex Company set id= " + execNumber + ", name = 'Company_" + execNumber + "'\n";
		cmd += "let owns = create edge Owns from $person to $company set name = 'Owns_" + execNumber + "'\n";
		
		cmd += "let city = create vertex City set id= " + execNumber + ", name = 'City_" + execNumber + "'\n";
		cmd += "let lives = create edge Lives from $person to $city set name = 'Lives_" + execNumber + "'\n";
		
		cmd += "commit retry 100\n";
		cmd += "return $owns;";

		OBasicLegacyResultSet edge = dbSession.command(new OCommandScript("sql", cmd)).execute();
		// edge.forEach(e -> System.out.println(e.toString()));
	}

	@Override
	protected String getTestName() {
		return REQUIREDEDGE.name() + "_APP";
	}
}
