package br.ufsc.lapesd.application;

import static java.lang.System.out;

import java.util.Date;

import javax.management.RuntimeErrorException;

import br.ufsc.lapesd.database.Configuration;
import br.ufsc.lapesd.database.TestType;

public class Application {
	private static long start_time;
	private static long end_time;

	public Application(TestType constraint, String csvPath) {
		System.out.printf(">>>>>>> Running as %s APPLICATION\n", constraint.name());
		AppTest test = getTestType(constraint);
		start_time = System.nanoTime();

		for (int execution = 0; execution < Configuration.NUM_EXECUTIONS; execution++) {
			out.println(">>>>>> Started execution: #" + execution + " " + new Date());
			test.run(execution);
			out.println(">>>>>> Ended execution: #" + execution + " " + new Date());
		}
		test.closeConnection();
		test.printcsv(csvPath);

		end_time = System.nanoTime();
		printStats(constraint.name());
	}

	private AppTest getTestType(TestType constraint) {
		switch (constraint) {
		case CARDINALITY:
			return new Cardinality();
		case CONDITIONAL:
			return new Conditional();
		case INOUT:
			return new InOut();
		case REQUIREDEDGE:
			return new RequiredEdge();
		default:
			throw new RuntimeErrorException(new Error(">>>>>>> Test mode not defined. Application will not run."));
		}
	}

	private static void printStats(String testName) {
		long exec_Time = (end_time - start_time) / 1000000000; // seconds
		out.println(">>>>>> Time spent on " + testName + " test: " + exec_Time + " seconds");
	}
}
