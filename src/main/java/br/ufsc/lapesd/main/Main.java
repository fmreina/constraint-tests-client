package br.ufsc.lapesd.main;

import static br.ufsc.lapesd.database.Configuration.CREATE_CONSTRAINT;
import static br.ufsc.lapesd.database.Configuration.CREATE_SCHEMA;
import static br.ufsc.lapesd.database.Configuration.CREATE_UNIQUE_INDEX;
import static br.ufsc.lapesd.database.Configuration.EXECUTE_INSERT;
import static br.ufsc.lapesd.database.Configuration.NUM_EXECUTIONS;
import static br.ufsc.lapesd.database.Configuration.NUM_INSERTS;
import static br.ufsc.lapesd.database.ExecMode.SCHEMA_APPLICATION;
import static br.ufsc.lapesd.database.ExecMode.SCHEMA_MODIFIED;
import static br.ufsc.lapesd.database.ExecMode.SCHEMA_ORIGINAL;
import static br.ufsc.lapesd.database.ExecMode.TEST_APPLICATION;
import static br.ufsc.lapesd.database.ExecMode.TEST_MODIFIED;
import static br.ufsc.lapesd.database.ExecMode.TEST_ORIGINAL;
import static br.ufsc.lapesd.database.TestType.UNIQUE;
import static java.lang.System.out;

import java.util.Date;

import javax.management.RuntimeErrorException;

import br.ufsc.lapesd.application.Application;
import br.ufsc.lapesd.constrainttests.CardinalityConstraintTest;
import br.ufsc.lapesd.constrainttests.ConditionalConstraintTest;
import br.ufsc.lapesd.constrainttests.ConstraintTest;
import br.ufsc.lapesd.constrainttests.InOutConstraintTest;
import br.ufsc.lapesd.constrainttests.RequiredConstraintTest;
import br.ufsc.lapesd.constrainttests.UniqueConstraintTest;
import br.ufsc.lapesd.database.Configuration;
import br.ufsc.lapesd.database.ExecMode;
import br.ufsc.lapesd.database.TestType;
import br.ufsc.lapesd.tests.v2.TestManagerV2;

/**
 * 
 * @author fabio
 *
 */
public class Main {

	private static long start_time;
	private static long end_time;
	private static long exec_Time;
	private static ExecMode testMode;
	private static TestType testType;
	private static ConstraintTest test;
	private static String csvPath;

	public static void main(String[] args) {
		checkTestParams(args);
		csvPath = args[2];
		adjustConfiguration(testMode);

		out.printf(">>>>> %s started..\n", testMode);
		out.printf(">>>>>> %s TEST\n", testType);
		out.println(">>>>> " + new Date());
		long start = System.nanoTime();

//		if (SCHEMA_APPLICATION.equals(testMode) || TEST_APPLICATION.equals(testMode)) {
//			new Application(testType, csvPath);
//		} else {
//			runTest(testType, csvPath);
//		}
		callTestManagerV2();

		long end = System.nanoTime();
		System.out.println(">>>>> " + new Date());
		out.printf(">>>>> %s finished..\n", testMode);
		exec_Time = (end - start) / 1000000000; // seconds
		out.println(">>>>> Total time spent: " + exec_Time + " seconds");
	}
	
	private static void callTestManagerV2(){
		System.out.println(String.format(">>>>> RUNNING ON %s MODE AND %s TEST TYPE", testMode, testType));
		new TestManagerV2(testMode, testType, csvPath);
	}

	private static void checkTestParams(String[] args) {
		if (args.length > 0)
			testMode = ExecMode.valueOf(args[0].toUpperCase());
		else
			System.err.println(
					"\n>>>>> Use one of the execution modes to run. Otherwise the execution will not take any effect.\nModes: SCHEMA = creates the schema, TEST = runs the inserts.");
		if (args.length > 1)
			testType = TestType.valueOf(args[1].toUpperCase());
		else
			System.err.println(
					"\n>>>>> Test type not informed. Use one: conditional, unique, inout, requirededge, cardinality");
	}

	private static void runTest(TestType type, String csvPath) {
		setTestType(type);
		execute(csvPath);
	}
	
	private static void setTestType(TestType type) {
		switch (type) {
		case CONDITIONAL:
			test = new ConditionalConstraintTest();
			break;
		case UNIQUE:
			test = new UniqueConstraintTest();
			break;
		case INOUT:
			test = new InOutConstraintTest();
			break;
		case REQUIREDEDGE:
			test = new RequiredConstraintTest();
			break;
		case CARDINALITY:
			test = new CardinalityConstraintTest();
			break;
		default:
			throw new RuntimeErrorException(new Error(">>>>> Test type not informed!"));
		}
	}

	private static void execute(String csvPath) {
		start_time = System.nanoTime();

		for (int execution = 0; execution < Configuration.NUM_EXECUTIONS; execution++) {
			out.println(">>>>> Started execution: #" + execution + " " + new Date());
			test.run(execution);
			out.println(">>>>> Ended execution: #" + execution + " " + new Date());
		}
		test.printcsv(csvPath);

		end_time = System.nanoTime();
		printStats(testType.name());
	}

	private static void printStats(String testName) {
		exec_Time = (end_time - start_time) / 1000000000; // seconds
		out.println(">>>>> Time spent on " + testName + " test: " + exec_Time + " seconds");
	}

	private static void adjustConfiguration(ExecMode mode) {
		if (mode.equals(SCHEMA_MODIFIED)) {
			out.println(">>>>> Executing on SCHEMA mode");
			NUM_EXECUTIONS = 1; 			// number of executions of <NUM_INSERTS> inserts
			NUM_INSERTS = 0; 				// number of inserts on each execution
			CREATE_SCHEMA = true;
			CREATE_CONSTRAINT = true;
			EXECUTE_INSERT = false;
		} else if (mode.equals(TEST_MODIFIED)) {
			out.println(">>>>> Executing on TEST mode");
			NUM_EXECUTIONS = 3;
			NUM_INSERTS = 100;
			CREATE_SCHEMA = false;
			CREATE_CONSTRAINT = false;
			EXECUTE_INSERT = true;
		} else if (mode.equals(SCHEMA_ORIGINAL)) {
			out.println(">>>>> Executing on SCHEMAORIG mode");
			NUM_EXECUTIONS = 1;
			NUM_INSERTS = 0;
			CREATE_SCHEMA = true;
			CREATE_CONSTRAINT = false;
			CREATE_UNIQUE_INDEX = testType == UNIQUE;
			EXECUTE_INSERT = false;
		} else if (mode.equals(TEST_ORIGINAL)) {
			out.println(">>>>> Executing on ORIGINAL mode");
			NUM_EXECUTIONS = 3;
			NUM_INSERTS = 100;
			CREATE_SCHEMA = false;
			CREATE_CONSTRAINT = false;
			EXECUTE_INSERT = true;
		} else if (mode.equals(SCHEMA_APPLICATION)) {
			out.println(">>>>> Executing on SCHEMA_APP mode");
			NUM_EXECUTIONS = 1;
			NUM_INSERTS = 100;
			CREATE_SCHEMA = true;
			EXECUTE_INSERT = false;
		} else if (mode.equals(TEST_APPLICATION)) {
			out.println(">>>>> Executing on TEST_APP mode");
			NUM_EXECUTIONS = 3;
			NUM_INSERTS = 100;
			CREATE_SCHEMA = false;
			EXECUTE_INSERT = true;
		} else {
			// out.println(">>>>> Error while setting execution mode: " + mode);
			throw new RuntimeErrorException(new Error(">>>>> Error while setting execution mode: " + mode));
		}
	}
}
