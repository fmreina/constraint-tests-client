package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.TestType.INOUT;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.application.AppTest;

public class InOutAppV2 extends AppTest{

	public InOutAppV2() {
		super();
		DATABASE_NAME = "InOut";
		createConnection();
		if (CREATE_SCHEMA) {
			createSchema();
		}
	}

	private void populate() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			Map<String, String> personProps = new HashMap<>();
			personProps.put("id", "p_" + i);
			personProps.put("name", "Person_" + i);

			Map<String, String> carProps1 = new HashMap<>();
			carProps1.put("id", "cp_1." + i);
			carProps1.put("name", "Company_1." + i);

			try {
				createVertex(dbSession, "Person", personProps);
				createVertex(dbSession, "Company", carProps1);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				personProps.clear();
				carProps1.clear();
			}
		}
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createEdge("Owns");
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", STRING);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
		if (edge == null)
			edge = dbSession.createEdgeClass(name);

		if (edge.getProperty("id") == null)
			edge.createProperty("id", STRING);
	}

	@Override
	public void run(int execution) {
		long startTime = 0;
		long endTime;
		beforeTest();

		if (EXECUTE_INSERT) {
			deleteAll();
			populate();
			startTime = System.nanoTime();
			createEntities(NUM_INSERTS, MOD); // will create edges
		}
		endTime = System.nanoTime();

		// afterRun();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void afterRun() {
		if (DELETE_ALL_RECORDS) {
			deleteAll();
		}

		if (DROP_DB)
			dropDatabase();
	}

	private void deleteAll() {
		dbSession.command("DELETE VERTEX Person");
		dbSession.command("DELETE VERTEX Company");
		dbSession.command("DELETE EDGE Owns");
	}

	@Override
	protected void createEntities(int numInserts, int mod) {
		for (int i = 0; i < numInserts; i++) {
			// System.out.println("Insert #" + i);
			OTransaction t = dbSession.getTransaction();
			ODatabaseDocument db = t.getDatabase();
			// try to create
			try {
				db.begin();

				OVertex person = queryNode(db, String.format("SELECT FROM Person WHERE id='%s';", "p_" + i));
				OVertex company1 = queryNode(db, String.format("SELECT FROM Company WHERE id='%s';", "cp_1." + i));

				if (validate(person, "Owns", company1)) {
					createEdgeOnTransaction(db, person, "Owns", company1);
					db.commit();
					countInserts++;
				} else
					throw new Exception(">>>>> Validation of INOUT failed.");
			} catch (Exception e) {
				countRejects++;
				db.rollback();
//				System.out.println("Rejected!\n" + e.getMessage());
			}
		}
	}

	private void createEdgeOnTransaction(ODatabaseDocument db, OVertex person, String edgeType, OVertex... cars) {
		for (OVertex car : cars) {
			db.command(
					String.format("CREATE EDGE %s FROM %s TO %s;", edgeType, person.getIdentity(), car.getIdentity()));
		}
	}

	private OVertex queryNode(ODatabaseDocument db, String query) {
		OResultSet rs = db.query(query);
		if (rs.hasNext()) {
			OResult result = rs.next();
			OElement element = result.toElement();
			Optional<OVertex> optional = element.asVertex();
			if (optional.isPresent())
				return optional.get();
		}
		return null;
	}

	public boolean validate(OVertex person, String edgeType, OVertex company) throws Exception {
		// CREATE CONSTRAINT inout ON Owns IN_OUT_EDGE FROM Person TO Company;
		if (("Owns").equalsIgnoreCase(edgeType)) {
			if (("Person").equalsIgnoreCase(((ODocument) person).getClassName())) {
				if (("Company").equalsIgnoreCase(((ODocument) company).getClassName())) {
					return true;
				} 
			}
		}
		return false;
	}

	@Override
	protected String getTestName() {
		return INOUT.name();
	}

}
