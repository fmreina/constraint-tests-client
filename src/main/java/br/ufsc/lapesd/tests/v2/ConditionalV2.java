package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.TestType.CONDITIONAL;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import com.orientechnologies.orient.core.metadata.schema.OClass;

import br.ufsc.lapesd.constrainttests.ConstraintTest;
import br.ufsc.lapesd.database.TestType;

public class ConditionalV2 extends ConstraintTest {

	public ConditionalV2() {
		sb = new StringBuilder(); // super();
		DATABASE_NAME = "Conditional";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	@Override
	protected void createSchema() {
		String name = "Person";
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", STRING);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);

		if (node.getProperty("attrib1") == null)
			node.createProperty("attrib1", INTEGER);

		if (node.getProperty("attrib2") == null)
			node.createProperty("attrib2", INTEGER);

		if (CREATE_CONSTRAINT)
			createConstraint();
	}

	@Override
	protected void createConstraint() {
		String name = "cond";
		String target = "Person";
		String type = "CONDITIONAL";

		String sql = "CREATE CONSTRAINT " + name + " ON " + target + " ( attrib1 ) " + type
				+ " ( IF attrib2 < 3 THEN attrib1 < 2 ELSE attrib1 > 4 )";

		dbSession.execute("sql", sql, "");
	}

	@Override
	public void run(int execution) {
		long startTime;
		long endTime;
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT)
			createEntities(NUM_INSERTS, MOD);
		endTime = System.nanoTime();

		afterRun();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void afterRun() {
		if (DELETE_ALL_RECORDS) {
			deleteAllEntities("Person");
		}

		if (DROP_DB)
			dropDatabase();
	}

	protected void createEntities(int numInserts, int mod) {
		Map<String, String> properties = new HashMap<>();
		String type = "Person";

		for (int i = 0; i < numInserts; i++) {
			properties.put("id", "" + i);
			properties.put("name", "Person_" + i);
			if (i % mod == 0) {
				// valid insert
				properties.put("attrib1", "1");
				properties.put("attrib2", "2");
			} else {
				// invalid insert
				properties.put("attrib1", "3");
				properties.put("attrib2", "4");
			}

			// try to create
			try {
				// VALIDATION happens in the server

				createVertex(dbSession, type, properties);
				countInserts++;
			} catch (Exception e) {
				countRejects++;
			} finally {
				properties.clear();
			}
		}
	}

	@Override
	@Deprecated
	protected void createEntities() {
		// Do nothing here
	}

	@Override
	protected TestType getTestName() {
		return CONDITIONAL;
	}

}
