package br.ufsc.lapesd.tests.v2;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.tx.OTransaction;

public class InOutV2OneCommand extends InOutV2 {

  @Override
  protected void createEntities(int numInserts, int mod) {
    for (int i = 0; i < numInserts; i++) {
      // System.out.println("Insert #" + i);
      OTransaction t = dbSession.getTransaction();
      ODatabaseDocument db = t.getDatabase();
      // try to create
      try {
        db.begin();
        
        db.command(
            String.format("CREATE EDGE Owns FROM "
                + "(SELECT FROM Person WHERE id='%s') TO "
                + "(SELECT FROM Company WHERE id='%s');",
                "p_" + i, "cr_1." + i) );

        // validation happens in the server

        db.commit();
        countInserts++;
      } catch (Exception e) {
        countRejects++;
        db.rollback();
        System.out.println("Rejected!\n" + e.getMessage());
      }
    }
  }
}
