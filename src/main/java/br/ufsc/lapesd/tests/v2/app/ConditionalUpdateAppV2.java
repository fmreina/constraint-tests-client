package br.ufsc.lapesd.tests.v2.app;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.tests.v2.ConditionalAppV2;

public class ConditionalUpdateAppV2 extends ConditionalAppV2 {

  @Override
  public void run(int execution) {
    long startTime = 0;
    long endTime;
    beforeTest();

    if (EXECUTE_INSERT) {
      deleteAll();
      populate();
      startTime = System.nanoTime();
      createEntities(NUM_INSERTS, MOD); // will update a property of the node
    }
    endTime = System.nanoTime();

    // afterRun();

    if (execution >= HEATING) // ignore the first 2 results
      updateStats(startTime, endTime);
  }

  @Override
  protected void createEntities(int numInserts, int mod) {
    for (int i = 0; i < numInserts; i++) {
   // System.out.println("update #" + i);
      OTransaction t = dbSession.getTransaction();
      ODatabaseDocument db = t.getDatabase();
      // try to update
      try {
        db.begin();
        
        OVertex person = queryNode(db, String.format("SELECT FROM PERSON WHERE id='%s';", "p_" + i));
        
        int attrib1_toUpdate = 0;
        
        if(validateAttrib1(person, attrib1_toUpdate)){
          db.command(String.format("UPDATE VERTEX Person SET attrib1=0 WHERE id='%s';", "p_" + i));
          db.commit();
          countInserts++;
        }
        else{
          throw new Exception(">>>>> Validation of CONDITIONAL UPDATE failed.");
        }
      } catch (Exception e) {
        countRejects++;
        db.rollback();
        // System.out.println("Rejected!\n" + e.getMessage());
      }
    }
  }

  private boolean validateAttrib1(OVertex person, int attrib1_toUpdate) {
    // IF attrib2 < 3 THEN attrib1 < 2 ELSE attrib1 > 4
    int attrib2 = person.getProperty("attrib2");
    
    if(attrib2 < 3){
      if (attrib1_toUpdate < 2) {
        return true;
      }
    } else {
      if (attrib1_toUpdate > 4) {
        return true;
      }
    }
    return false;
  }

  private void populate() {
    Map<String, String> properties = new HashMap<>();
    String type = "Person";

    for (int i = 0; i < NUM_INSERTS; i++) {
      properties.put("id", "p_" + i);
      properties.put("name", "Person_" + i);
      properties.put("attrib1", "1");
      properties.put("attrib2", "2");

      // try to create
      try {
        createVertex(dbSession, type, properties);
      } catch (Exception e) {
        System.out.println(e.getMessage());
      } finally {
        properties.clear();
      }
    }
  }

  private void deleteAll() {
    dbSession.command("DELETE VERTEX Person");
  }

  private OVertex queryNode(ODatabaseDocument db, String query) {
    OResultSet rs = db.query(query);
    if (rs.hasNext()) {
      OResult result = rs.next();
      OElement element = result.toElement();
      Optional<OVertex> optional = element.asVertex();
      if (optional.isPresent())
        return optional.get();
    }
    return null;
  }
}
