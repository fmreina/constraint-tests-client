package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.TestType.REQUIREDEDGE;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.ODirection;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.OVertexDocument;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.application.AppTest;

public class RequiredEdgeAppV2 extends AppTest {

	public RequiredEdgeAppV2() {
		super();
		DATABASE_NAME = "RequiredEdge";
		createConnection();
		if (CREATE_SCHEMA) {
			createSchema();
		}
	}

	private void populate() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			Map<String, String> personProps = new HashMap<>();
			personProps.put("id", "p_" + i);
			personProps.put("name", "Person_" + i);

			Map<String, String> companyProps = new HashMap<>();
			companyProps.put("id", "cp_" + i);
			companyProps.put("name", "Company_" + i);

			Map<String, String> cityProps = new HashMap<>();
			cityProps.put("id", "ct_" + i);
			cityProps.put("name", "City_" + i);

			Map<String, String> ownsProps = new HashMap<>();
			ownsProps.put("id", "o_" + i);

			Map<String, String> livesProps = new HashMap<>();
			livesProps.put("id", "l_" + i);

			try {
				OVertex person = createVertex(dbSession, "Person", personProps);
				OVertex company = createVertex(dbSession, "Company", companyProps);
				OVertex city = createVertex(dbSession, "City", cityProps);
				createEdgeWithProperties(dbSession, person, company, "Owns", ownsProps);
				createEdgeWithProperties(dbSession, person, city, "Lives", livesProps);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				personProps.clear();
				companyProps.clear();
				cityProps.clear();
				ownsProps.clear();
				livesProps.clear();
			}
		}
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Lives");
		createEdge("Owns");
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", STRING);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
		if (edge == null)
			edge = dbSession.createEdgeClass(name);

		if (edge.getProperty("id") == null)
			edge.createProperty("id", STRING);
	}

	@Override
	public void run(int execution) {
		long startTime = 0;
		long endTime;
		beforeTest();

		if (EXECUTE_INSERT) {
			deleteAll();
			populate();
			startTime = System.nanoTime();
			createEntities(NUM_INSERTS, MOD); // will delete edges
		}
		endTime = System.nanoTime();

		// afterRun();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void afterRun() {
		if (DELETE_ALL_RECORDS) {
			deleteAll();
		}

		if (DROP_DB)
			dropDatabase();
	}

	private void deleteAll() {
		dbSession.command("DELETE VERTEX Person");
		dbSession.command("DELETE VERTEX Company");
		dbSession.command("DELETE VERTEX City");
		dbSession.command("DELETE EDGE Owns");
		dbSession.command("DELETE EDGE Lives");
	}

	@Override
	protected void createEntities(int numInserts, int mod) {
		for (int i = 0; i < numInserts; i++) {
			// System.out.println("Delete #" + i);
			OTransaction t = dbSession.getTransaction();
			ODatabaseDocument db = t.getDatabase();
			// try to delete
			try {
				db.begin();
				OEdge lives = queryEdge(db, String.format("SELECT FROM Lives WHERE id='%s';", "l_" + i));

				if (validate(lives)) {
					db.command(String.format("DELETE EDGE Lives WHERE id='%s';", "l_" + i));
					db.commit();
					countInserts++;
				} else
					throw new Exception(">>>>> Validation of REQUIREDEDE failed.");
			} catch (Exception e) {
				countRejects++;
				db.rollback();
				System.out.println("Rejected!\n" + e.getMessage());
			}
		}
	}

	private OEdge queryEdge(ODatabaseDocument db, String query) {
		OResultSet rs = db.query(query);
		if (rs.hasNext()) {
			OResult result = rs.next();
			OElement element = result.toElement();
			Optional<OEdge> optional = element.asEdge();
			if (optional.isPresent())
				return optional.get();
		}
		return null;
	}

	public boolean validate(OEdge lives) throws Exception {
		OVertex person = lives.getVertex(ODirection.OUT);
		OVertex city = lives.getVertex(ODirection.IN);

		OVertexDocument personDoc = (OVertexDocument) person;
		OVertexDocument cityDoc = (OVertexDocument) city;

		if (("Person").equalsIgnoreCase(personDoc.getClassName()) && ("Company").equalsIgnoreCase(cityDoc.getClassName()))
			return false;
		return true;
	}

	@Override
	protected String getTestName() {
		return REQUIREDEDGE.name();
	}

}
