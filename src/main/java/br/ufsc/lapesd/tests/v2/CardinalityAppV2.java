package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.TestType.CARDINALITY;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.db.record.ridbag.ORidBag;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.application.AppTest;

public class CardinalityAppV2 extends AppTest {

	public CardinalityAppV2() {
		super();
		DATABASE_NAME = "Cardinality";
		createConnection();
		if (CREATE_SCHEMA) {
			createSchema();
		}
	}

	private void populate() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			Map<String, String> personProps = new HashMap<>();
			personProps.put("id", "p_" + i);
			personProps.put("name", "Person_" + i);

			Map<String, String> companyProps1 = new HashMap<>();
			companyProps1.put("id", "cp_1." + i);
			companyProps1.put("name", "Company_1." + i);

			Map<String, String> companyProps2 = new HashMap<>();
			companyProps2.put("id", "cp_2." + i);
			companyProps2.put("name", "Company_2." + i);

			Map<String, String> companyProps3 = new HashMap<>();
			companyProps3.put("id", "cp_3." + i);
			companyProps3.put("name", "Company_3." + i);

			Map<String, String> companyProps4 = new HashMap<>();
			companyProps4.put("id", "cp_4." + i);
			companyProps4.put("name", "Company_4." + i);

			try {
				createVertex(dbSession, "Person", personProps);
				createVertex(dbSession, "Company", companyProps1);
				createVertex(dbSession, "Company", companyProps2);
				createVertex(dbSession, "Company", companyProps3);
				// createVertex(dbSession, "Company", companyProps4);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				personProps.clear();
				companyProps1.clear();
				companyProps2.clear();
				companyProps3.clear();
				companyProps4.clear();
			}
		}
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createEdge("Owns");
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", STRING);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
		if (edge == null)
			edge = dbSession.createEdgeClass(name);

		if (edge.getProperty("id") == null)
			edge.createProperty("id", STRING);
	}

	@Override
	public void run(int execution) {
		long startTime = 0;
		long endTime;
		beforeTest();

		if (EXECUTE_INSERT) {
			deleteAll();
			populate();
			startTime = System.nanoTime();
			createEntities(NUM_INSERTS, MOD); // will create edges
		}
		endTime = System.nanoTime();

		// afterRun();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void afterRun() {
		if (DELETE_ALL_RECORDS) {
			deleteAll();
		}

		if (DROP_DB)
			dropDatabase();
	}

	private void deleteAll() {
		dbSession.command("DELETE VERTEX Person");
		dbSession.command("DELETE VERTEX Company");
		dbSession.command("DELETE EDGE Owns");
	}

	@Override
	protected void createEntities(int numInserts, int mod) {
		for (int i = 0; i < numInserts; i++) {
			// System.out.println("Insert #" + i);
			OTransaction t = dbSession.getTransaction();
			ODatabaseDocument db = t.getDatabase();
			// try to create
			try {
				db.begin();

				OVertex person = queryNode(db, String.format("SELECT FROM Person WHERE id='%s';", "p_" + i));
				OVertex company1 = queryNode(db, String.format("SELECT FROM Company WHERE id='%s';", "cp_1." + i));
				OVertex company2 = queryNode(db, String.format("SELECT FROM Company WHERE id='%s';", "cp_2." + i));
				OVertex company3 = queryNode(db, String.format("SELECT FROM Company WHERE id='%s';", "cp_3." + i));

				if (validate(person, "Owns", company1, company2, company3)) {
					createEdgeOnTransaction(db, person, "Owns", company1, company2, company3);
					db.commit();
					countInserts++;
				} else
					throw new Exception(">>>>> Validation of CARDINALITY failed.");
			} catch (Exception e) {
				countRejects++;
				db.rollback();
				// System.out.println("Rejected!\n" + e.getMessage());
			}
		}
	}

	private void createEdgeOnTransaction(ODatabaseDocument db, OVertex person, String edgeType, OVertex... companies) {
		for (OVertex ccompany : companies) {
			db.command(
					String.format("CREATE EDGE %s FROM %s TO %s;", edgeType, person.getIdentity(), ccompany.getIdentity()));
		}
	}

	private OVertex queryNode(ODatabaseDocument db, String query) {
		OResultSet rs = db.query(query);
		if (rs.hasNext()) {
			OResult result = rs.next();
			OElement element = result.toElement();
			Optional<OVertex> optional = element.asVertex();
			if (optional.isPresent())
				return optional.get();
		}
		return null;
	}

	public boolean validate(OVertex person, String edgeType, OVertex... companies) throws Exception {
		int pLimit = 3;
		int cLimit = 99999; // represents the lack of limit 'N'

		if (("Owns").equalsIgnoreCase(edgeType)) {
			if (("Person").equalsIgnoreCase(((ODocument) person).getClassName())) {
				ORidBag outOwns = person.getProperty("out_Owns");
				if (outOwns != null && outOwns.size() >= pLimit) {
					return false;
				}

				int counter = 0;
				for (OVertex company : companies) {
					if (("Company").equalsIgnoreCase(((ODocument) company).getClassName())) {
						ORidBag inOwns = company.getProperty("in_Owns");
						if (inOwns != null && inOwns.size() >= cLimit)
							return false;
						counter++;
					} else
						return false;
				}

				if (counter > pLimit)
					return false;

				return true;
			}
		}
		return false;
	}

	@Override
	protected String getTestName() {
		return CARDINALITY.name();
	}

}
