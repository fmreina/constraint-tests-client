package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.ExecMode.SCHEMA_APPLICATION;
import static br.ufsc.lapesd.database.ExecMode.TEST_APPLICATION;
import static java.lang.System.out;

import java.util.Date;

import br.ufsc.lapesd.application.AppTest;
import br.ufsc.lapesd.constrainttests.ConstraintTest;
import br.ufsc.lapesd.database.Configuration;
import br.ufsc.lapesd.database.ExecMode;
import br.ufsc.lapesd.database.TestType;
import br.ufsc.lapesd.tests.v2.app.ConditionalUpdateAppV2;
import br.ufsc.lapesd.tests.v2.app.ConditionalUpdateV2;

public class TestManagerV2 {

	public TestManagerV2(ExecMode testMode, TestType testType, String csvPath) {
		long start_time = System.nanoTime();
		try {
			if (SCHEMA_APPLICATION.equals(testMode) || TEST_APPLICATION.equals(testMode)) {
				AppTest test = getAppTestType(testType);

				if (test != null) {
					for (int execution = 0; execution < Configuration.NUM_EXECUTIONS; execution++) {
						out.println(">>>>>> Started execution: #" + execution + " " + new Date());
						test.run(execution);
						out.println(">>>>>> Ended execution: #" + execution + " " + new Date());
					}

					test.closeConnection();
					test.printcsv(csvPath);
				}
			} else {
				ConstraintTest test = getTestType(testType);

				if (test != null) {
					for (int execution = 0; execution < Configuration.NUM_EXECUTIONS; execution++) {
						out.println(">>>>>> Started execution: #" + execution + " " + new Date());
						test.run(execution);
						out.println(">>>>>> Ended execution: #" + execution + " " + new Date());
					}

					test.closeConnection();
					test.printcsv(csvPath);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			long end_time = System.nanoTime();
			printStatsOnConsole(testType.name(), start_time, end_time);
		}
	}

	private AppTest getAppTestType(TestType type) throws Exception {
		switch (type) {
		case CONDITIONAL:
//			return new ConditionalAppV2();
		  return new ConditionalUpdateAppV2();
		case INOUT:
			 return new InOutAppV2();
		case REQUIREDEDGE:
			 return new RequiredEdgeAppV2();
		case CARDINALITY:
			 return new CardinalityAppV2();
		default:
			throw new Exception(">>>>> Test type not informed!");
		}
	}

	private ConstraintTest getTestType(TestType type) throws Exception {
		switch (type) {
		case CONDITIONAL:
//			return new ConditionalV2();
		  return new ConditionalUpdateV2();
		case INOUT:
//			 return new InOutV2();
		  return new InOutV2OneCommand();
		case REQUIREDEDGE:
			 return new RequiredEdgeV2();
		case CARDINALITY:
			 return new CardinalityV2();
		default:
			throw new Exception(">>>>> Test type not informed!");
		}
	}

	private static void printStatsOnConsole(String testName, long start_time, long end_time) {
		long exec_Time = (end_time - start_time) / 1000000000; // seconds
		out.println(">>>>>> Time spent on " + testName + " test: " + exec_Time + " seconds");
	}
}
