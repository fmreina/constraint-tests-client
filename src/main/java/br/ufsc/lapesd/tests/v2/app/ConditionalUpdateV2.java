package br.ufsc.lapesd.tests.v2.app;

import java.util.HashMap;
import java.util.Map;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.tests.v2.ConditionalV2;

public class ConditionalUpdateV2 extends ConditionalV2 {

  @Override
  public void run(int execution) {
    long startTime = 0;
    long endTime;
    beforeTest();

    if (EXECUTE_INSERT) {
      deleteAll();
      populate();
      startTime = System.nanoTime();
      createEntities(NUM_INSERTS, MOD); // will update a property of the node
    }
    endTime = System.nanoTime();

    // afterRun();

    if (execution >= HEATING) // ignore the first 2 results
      updateStats(startTime, endTime);
  }

  @Override
  protected void createEntities(int numInserts, int mod) {
    for (int i = 0; i < numInserts; i++) {
      // System.out.println("update #" + i);
      OTransaction t = dbSession.getTransaction();
      ODatabaseDocument db = t.getDatabase();
      // try to update
      try {
        db.begin();

        db.command(String.format("UPDATE Person SET attrib1=0 WHERE id='%s';", "p_" + i));
        db.commit();
//        dbSession.command(String.format("UPDATE Person SET attrib1=0 WHERE id='%s';", "p_" + i));
        countInserts++;
      } catch (Exception e) {
        countRejects++;
        db.rollback();
         System.out.println("Rejected!\n" + e.getMessage());
      }
    }
  }

  private void populate() {
    Map<String, String> properties = new HashMap<>();
    String type = "Person";

    for (int i = 0; i < NUM_INSERTS; i++) {
      properties.put("id", "p_" + i);
      properties.put("name", "Person_" + i);
      properties.put("attrib1", "1");
      properties.put("attrib2", "2");

      // try to create
      try {
        createVertex(dbSession, type, properties);
      } catch (Exception e) {
        System.out.println(e.getMessage());
      } finally {
        properties.clear();
      }
    }
  }

  private void deleteAll() {
    dbSession.command("DELETE VERTEX Person");
  }
}
