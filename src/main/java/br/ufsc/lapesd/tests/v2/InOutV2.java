package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.TestType.INOUT;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.constrainttests.ConstraintTest;
import br.ufsc.lapesd.database.TestType;

public class InOutV2 extends ConstraintTest{

	public InOutV2() {
		sb = new StringBuilder(); // super();
		DATABASE_NAME = "InOut";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	private void populate() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			Map<String, String> personProps = new HashMap<>();
			personProps.put("id", "p_" + i);
			personProps.put("name", "Person_" + i);

			Map<String, String> companyProps1 = new HashMap<>();
			companyProps1.put("id", "cr_1." + i);
			companyProps1.put("name", "Company_1." + i);

			try {
				createVertex(dbSession, "Person", personProps);
				createVertex(dbSession, "Company", companyProps1);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				personProps.clear();
				companyProps1.clear();
			}
		}
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createEdge("Owns");

		if (CREATE_CONSTRAINT)
			createConstraint();
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", STRING);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
		if (edge == null)
			edge = dbSession.createEdgeClass(name);

		if (edge.getProperty("id") == null)
			edge.createProperty("id", STRING);
	}

	@Override
	protected void createConstraint() {
		String name = "inout";
		String target = "Owns";
		String type = "IN_OUT_EDGE";
		String origin = "Person";
		String destiny = "Company";
		
		String sql = "CREATE CONSTRAINT " + name
				+ " ON " + target + " " + type 
				+ " FROM " + origin + " TO " + destiny;
		
		dbSession.execute("sql", sql, "");
	}

	@Override
	public void run(int execution) {
		long startTime = 0;
		long endTime;
		beforeTest();

		if (EXECUTE_INSERT) {
			deleteAll();
			populate();
			startTime = System.nanoTime();
			createEntities(NUM_INSERTS, MOD); // will create edges
		}
		endTime = System.nanoTime();

		// afterRun();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void afterRun() {
		if (DELETE_ALL_RECORDS) {
			deleteAll();
		}

		if (DROP_DB)
			dropDatabase();
	}

	private void deleteAll() {
		dbSession.command("DELETE VERTEX Person");
		dbSession.command("DELETE VERTEX Company");
		dbSession.command("DELETE EDGE Owns");
	}

	protected void createEntities(int numInserts, int mod) {
		for (int i = 0; i < numInserts; i++) {
			// System.out.println("Insert #" + i);
			OTransaction t = dbSession.getTransaction();
			ODatabaseDocument db = t.getDatabase();
			// try to create
			try {
				db.begin();

				OVertex person = queryNode(db, String.format("SELECT FROM Person WHERE id='%s';", "p_" + i));
				OVertex company1 = queryNode(db, String.format("SELECT FROM Company WHERE id='%s';", "cr_1." + i));

				// validation happens in the server
				
				createEdgeOnTransaction(db, person, "Owns", company1);
				db.commit();
				countInserts++;
			} catch (Exception e) {
				countRejects++;
				db.rollback();
				System.out.println("Rejected!\n" + e.getMessage());
			}
		}
	}

	private void createEdgeOnTransaction(ODatabaseDocument db, OVertex person, String edgeType, OVertex... companies) {
		for (OVertex company : companies) {
			db.command(
					String.format("CREATE EDGE %s FROM %s TO %s;", edgeType, person.getIdentity(), company.getIdentity()));
		}
	}

	private OVertex queryNode(ODatabaseDocument db, String query) {
		OResultSet rs = db.query(query);
		if (rs.hasNext()) {
			OResult result = rs.next();
			OElement element = result.toElement();
			Optional<OVertex> optional = element.asVertex();
			if (optional.isPresent())
				return optional.get();
		}
		return null;
	}

	@Override
	@Deprecated
	protected void createEntities() {
		// Do nothing here
	}

	@Override
	protected TestType getTestName() {
		return INOUT;
	}
}
