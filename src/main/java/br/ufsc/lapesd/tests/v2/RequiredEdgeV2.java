package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.TestType.REQUIREDEDGE;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.constrainttests.ConstraintTest;
import br.ufsc.lapesd.database.TestType;

public class RequiredEdgeV2 extends ConstraintTest {

	public RequiredEdgeV2() {
		sb = new StringBuilder(); // super();
		DATABASE_NAME = "RequiredEdge";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	private void populate() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			Map<String, String> personProps = new HashMap<>();
			personProps.put("id", "p_" + i);
			personProps.put("name", "Person_" + i);

			Map<String, String> companyProps = new HashMap<>();
			companyProps.put("id", "cp_1." + i);
			companyProps.put("name", "Company_" + i);
			
			Map<String, String> companyProps2 = new HashMap<>();
			companyProps2.put("id", "cp_2." + i);
			companyProps2.put("name", "Company_2." + i);

			Map<String, String> cityProps = new HashMap<>();
			cityProps.put("id", "ct_" + i);
			cityProps.put("name", "City_" + i);

			Map<String, String> ownsProps1 = new HashMap<>();
			ownsProps1.put("id", "o_1." + i);
			
			Map<String, String> ownsProps2 = new HashMap<>();
			ownsProps2.put("id", "o_2." + i);

			Map<String, String> livesProps = new HashMap<>();
			livesProps.put("id", "l_" + i);

			try {
				OVertex person = createVertex(dbSession, "Person", personProps);
				OVertex company = createVertex(dbSession, "Company", companyProps);
				OVertex company2 = createVertex(dbSession, "Company", companyProps2);
				OVertex city = createVertex(dbSession, "City", cityProps);
				createEdgeWithProperties(dbSession, person, company, "Owns", ownsProps1);
				createEdgeWithProperties(dbSession, person, company2, "Owns", ownsProps2);
				createEdgeWithProperties(dbSession, person, city, "Lives", livesProps);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				personProps.clear();
				companyProps.clear();
				companyProps2.clear();
				cityProps.clear();
				ownsProps1.clear();
				ownsProps2.clear();
				livesProps.clear();
			}
		}
	}

	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Lives");
		createEdge("Owns");

		if (CREATE_CONSTRAINT)
			createConstraint();
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", STRING);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
		if (edge == null)
			edge = dbSession.createEdgeClass(name);

		if (edge.getProperty("id") == null)
			edge.createProperty("id", STRING);
	}

	@Override
	protected void createConstraint() {
		String name = "req";
		String target = "Person";
		String type = "REQUIRED_EDGE";
		String edgeType = "Owns";
		String destiny = "Company";

		String sql = "CREATE CONSTRAINT " + name + " ON " + target + " " + type + " " + edgeType + " TO " + destiny;

		dbSession.execute("sql", sql, "");
	}

	@Override
	public void run(int execution) {
		long startTime = 0;
		long endTime;
		beforeTest();

		if (EXECUTE_INSERT) {
			deleteAll();
			populate();
			startTime = System.nanoTime();
			createEntities(NUM_INSERTS, MOD); // will delete edges
		}
		endTime = System.nanoTime();

		// afterRun();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void afterRun() {
		if (DELETE_ALL_RECORDS) {
			deleteAll();
		}

		if (DROP_DB)
			dropDatabase();
	}

	private void deleteAll() {
		dbSession.command("DELETE VERTEX Person");
		dbSession.command("DELETE VERTEX Company");
		dbSession.command("DELETE VERTEX City");
		dbSession.command("DELETE EDGE Owns");
		dbSession.command("DELETE EDGE Lives");
	}

	protected void createEntities(int numInserts, int mod) {
		for (int i = 0; i < numInserts; i++) {
			// System.out.println("Delete #" + i);
			OTransaction t = dbSession.getTransaction();
			ODatabaseDocument db = t.getDatabase();
			// try to delete
			try {
				db.begin();

				// validation happens in the server

//				db.command(String.format("DELETE EDGE Lives WHERE id='%s';", "l_" + i));
				db.command(String.format("DELETE EDGE Owns WHERE id='%s';", "o_2." + i));
//				db.command(String.format("DELETE EDGE Owns WHERE id='%s';", "o_1." + i));
				db.commit();
				countInserts++;
			} catch (Exception e) {
				countRejects++;
				db.rollback();
				System.out.println("Rejected!\n" + e.getMessage());
			}
		}
	}

	@Override
	@Deprecated
	protected void createEntities() {
		// Do nothing here
	}

	@Override
	protected TestType getTestName() {
		return REQUIREDEDGE;
	}

}
