package br.ufsc.lapesd.tests.v2;

import static br.ufsc.lapesd.database.TestType.CARDINALITY;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import com.orientechnologies.orient.core.command.script.OCommandScript;
import com.orientechnologies.orient.core.db.document.ODatabaseDocument;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.sql.query.OBasicLegacyResultSet;
import com.orientechnologies.orient.core.tx.OTransaction;

import br.ufsc.lapesd.constrainttests.ConstraintTest;
import br.ufsc.lapesd.database.TestType;

public class CardinalityV2 extends ConstraintTest {

	public CardinalityV2() {
		sb = new StringBuilder(); // super();
		DATABASE_NAME = "Cardinality";
		createConnection();
		if (CREATE_SCHEMA)
			createSchema();
	}

	private void populate() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			Map<String, String> personProps = new HashMap<>();
			personProps.put("id", "p_" + i);
			personProps.put("name", "Person_" + i);

			Map<String, String> companyProps1 = new HashMap<>();
			companyProps1.put("id", "cp_1." + i);
			companyProps1.put("name", "Company_1." + i);

			Map<String, String> companyProps2 = new HashMap<>();
			companyProps2.put("id", "cp_2." + i);
			companyProps2.put("name", "Company_2." + i);

			Map<String, String> companyProps3 = new HashMap<>();
			companyProps3.put("id", "cp_3." + i);
			companyProps3.put("name", "Company_3." + i);

//			Map<String, String> companyProps4 = new HashMap<>();
//			companyProps4.put("id", "cp_4." + i);
//			companyProps4.put("name", "Company_4." + i);

			try {
				createVertex(dbSession, "Person", personProps);
				createVertex(dbSession, "Company", companyProps1);
				createVertex(dbSession, "Company", companyProps2);
				createVertex(dbSession, "Company", companyProps3);
//				 createVertex(dbSession, "Company", companyProps4);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			} finally {
				personProps.clear();
				companyProps1.clear();
				companyProps2.clear();
				companyProps3.clear();
//				companyProps4.clear();
			}
		}
	}

	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createEdge("Owns");

		if (CREATE_CONSTRAINT)
			createConstraint();
	}

	private void createNode(String name) {
		OClass node = dbSession.getClass(name);
		if (node == null)
			node = dbSession.createVertexClass(name);

		if (node.getProperty("id") == null)
			node.createProperty("id", STRING);

		if (node.getProperty("name") == null)
			node.createProperty("name", STRING);
	}

	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
		if (edge == null)
			edge = dbSession.createEdgeClass(name);

		if (edge.getProperty("id") == null)
			edge.createProperty("id", STRING);
	}

	@Override
	protected void createConstraint() {
		String name = "card";
		String target = "Person";
		String type = "CARDINALITY";
		String edgeType = "Owns";
		String lowerLimit = "N";
		String upperLimit = "3";
		String destiny = "Company";

		String sql = "CREATE CONSTRAINT " + name + " ON " + target + " " + type + " " + edgeType + " " + lowerLimit
				+ " .. " + upperLimit + " TO " + destiny;

		dbSession.execute("sql", sql, "");
	}

	@Override
	public void run(int execution) {
		long startTime = 0;
		long endTime;
		beforeTest();

		if (EXECUTE_INSERT) {
			deleteAll();
			populate();
			startTime = System.nanoTime();
			createEntities(NUM_INSERTS, MOD); // will create edges
		}
		endTime = System.nanoTime();

		// afterRun();

		if (execution >= HEATING) // ignore the first 2 results
			updateStats(startTime, endTime);
	}

	@Override
	protected void afterRun() {
		if (DELETE_ALL_RECORDS) {
			deleteAll();
		}

		if (DROP_DB)
			dropDatabase();
	}

	private void deleteAll() {
		dbSession.command("DELETE VERTEX Person");
		dbSession.command("DELETE VERTEX Company");
		dbSession.command("DELETE EDGE Owns");
	}

	protected void createEntities(int numInserts, int mod) {
		for (int i = 0; i < numInserts; i++) {
			// try to create
			try {
				// validation happens on server

				String cmd = "begin\n";
				cmd += "let person = SELECT FROM Person WHERE id='p_"+i+"'\n";
				cmd += "let company1 = SELECT FROM Company WHERE id='cp_1."+i+"'\n";
				cmd += "let company2 = SELECT FROM Company WHERE id='cp_2."+i+"'\n";
				cmd += "let company3 = SELECT FROM Company WHERE id='cp_3."+i+"'\n";
				
				cmd += "let owns1 = create edge Owns from $person to $company1\n";
				cmd += "let owns2 = create edge Owns from $person to $company2\n";
				cmd += "let owns3 = create edge Owns from $person to $company3\n";

//				cmd += "let company4 = SELECT FROM Company WHERE id='cp_4."+i+"'\n";
//				cmd += "let owns4 = create edge Owns from $person to $company4\n";
				
				cmd += "commit retry 100\n";
				cmd += "return $owns1";

				OTransaction t = dbSession.getTransaction();
				ODatabaseDocument db = t.getDatabase();
				
				OBasicLegacyResultSet edge = db.command(new OCommandScript("sql", cmd)).execute();
				countInserts++;
			} catch (Exception e) {
				countRejects++;
				System.out.println(e.getMessage());
			} finally {
			}
		}
	}

	@Override
	@Deprecated
	protected void createEntities() {
		// Do nothing here
	}

	@Override
	protected TestType getTestName() {
		return CARDINALITY;
	}
}
