package br.ufsc.lapesd.constrainttests;

import static br.ufsc.lapesd.database.TestType.REQUIREDEDGE;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import com.orientechnologies.orient.core.command.script.OCommandScript;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.sql.query.OBasicLegacyResultSet;

import br.ufsc.lapesd.database.TestType;

/**
 * 
 * @author fabio
 *
 */
public class RequiredConstraintTest extends ConstraintTest {

	public RequiredConstraintTest() {
		DATABASE_NAME = "RequiredEdge";
	}
	
	@Override
	protected TestType getTestName() {
		return REQUIREDEDGE;
	}
	
	public void run(int execution){
		super.run(execution);
		long startTime;
		long endTime;

		createConnection();
		
		if (CREATE_SCHEMA) createSchema();
		
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT) createEntities();
		endTime = System.nanoTime();

		if (DELETE_ALL_RECORDS) {
			deleteAllEntities("Person");
			deleteAllEntities("Company");
		}

		closeConnection();
		
		if (DROP_DB) dropDatabase();

		if(execution >= HEATING) // ignore the first 2 results for heating the JVM
			updateStats(startTime, endTime);
	}
	
	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Owns");
		if(CREATE_CONSTRAINT)
			createConstraint(); // it should run only once
	}
	
	private void createNode(String name) {

	    OClass node = dbSession.getClass(name);
	    if (node == null)
	      node = dbSession.createVertexClass(name);
	    
	    if (node.getProperty("id") == null)
	    	node.createProperty("id", INTEGER);

	    if (node.getProperty("name") == null)
	      node.createProperty("name", STRING);

	}
	
	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
	    if (edge == null)
	      edge = dbSession.createEdgeClass(name);
	}
	
	@Override
	protected void createConstraint() {
		String name = "req";
		String target = "Person";
		String type = "REQUIRED_EDGE";
		String edgeType = "Owns";
		String destiny = "Company";
		
		String sql = "CREATE CONSTRAINT " + name
				+ " ON " + target + " " + type 
				+ " " + edgeType + " TO " + destiny;
		
		dbSession.execute("sql", sql, "");
	}

	@Override
	protected void createEntities() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			try {
				if (i % MOD == 0) {
					// valid insert
					batchCreateValid(i);
				} else {
					// invalid insert
					batchCreateInvalid(i);
				}
				countInserts++;
			} catch (Exception e) {
				
				countRejects++;
			}
		}
	}
	
	private void batchCreateValid(int execNumber) {
		String cmd = "begin\n";
		cmd += "let person = create vertex Person set id= " + execNumber + ", name = 'Person_" + execNumber + "'\n";
		cmd += "let company = create vertex Company set id= " + execNumber + ", name = 'Company_" + execNumber + "'\n";
		cmd += "let owns = create edge Owns from $person to $company\n";
		cmd += "commit retry 100\n";
		cmd += "return $owns";

		OBasicLegacyResultSet edge = dbSession.command(new OCommandScript("sql", cmd)).execute();
		// edge.forEach(e -> System.out.println(e.toString()));
	}

	private void batchCreateInvalid(int execNumber) {
		String cmd = "begin\n";
		cmd += "let person = create vertex Person set id= " + execNumber + ", name = 'Person_" + execNumber + "'\n";
		cmd += "let company = create vertex Company set id= " + execNumber + ", name = 'Company_" + execNumber + "'\n";
		cmd += "let owns = create edge Owns from $company to $person\n";
		cmd += "commit retry 100\n";
		cmd += "return $owns";

		OBasicLegacyResultSet edge = dbSession.command(new OCommandScript("sql", cmd)).execute();
		// edge.forEach(e -> System.out.println(e.toString()));
	}
}
