package br.ufsc.lapesd.constrainttests;

import static br.ufsc.lapesd.database.TestType.CONDITIONAL;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import com.orientechnologies.orient.core.metadata.schema.OClass;

import br.ufsc.lapesd.database.TestType;

/**
 * 
 * @author fabio
 *
 */
public class ConditionalConstraintTest extends ConstraintTest {

	public ConditionalConstraintTest() {
		DATABASE_NAME = "Conditional";
	}
	
	@Override
	protected TestType getTestName() {
		return CONDITIONAL;
	}
	
	public void run(int execution){
		super.run(execution);
		long startTime;
		long endTime;
		countInserts = 0;
		countRejects = 0;
		
		createConnection();
		
		if (CREATE_SCHEMA) createSchema();
		
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT) createEntities();
		endTime = System.nanoTime();

		if (DELETE_ALL_RECORDS) deleteAllEntities("Person");

		closeConnection();
		
		if (DROP_DB) dropDatabase();

		if(execution >= HEATING) // ignore the first 2 results for heating the JVM
			updateStats(startTime, endTime);
	}
	
	@Override
	protected void createSchema() {
		createNode("Person");
		if(CREATE_CONSTRAINT)
			createConstraint(); // it should run only once
	}
	
	private void createNode(String name) {
	    OClass node = dbSession.getClass(name);
	    if (node == null)
	      node = dbSession.createVertexClass(name);
	    
	    if (node.getProperty("id") == null)
	    	node.createProperty("id", INTEGER);

	    if (node.getProperty("name") == null)
	      node.createProperty("name", STRING);

	    if (node.getProperty("attrib1") == null)
	      node.createProperty("attrib1", INTEGER);

	    if (node.getProperty("attrib2") == null)
		      node.createProperty("attrib2", INTEGER);
	}
	
	@Override
	protected void createConstraint() {
		String name = "cond";
		String target = "Person";
		String type = "CONDITIONAL";
		
		String sql = "CREATE CONSTRAINT " + name
				+ " ON " + target + " ( attrib1 ) " + type 
				+ " ( IF attrib2 < 3 THEN attrib1 < 2 ELSE attrib1 > 4 )";
		
		dbSession.execute("sql", sql, "");
	}

	@Override
	protected void createEntities() {
		Map<String, String> properties = new HashMap<>();
		String type = "Person";

		for (int i = 0; i < NUM_INSERTS; i++) {
			properties.put("id", "" + i);
			properties.put("name", "Person_" + i);
			if (i % MOD == 0) {
				// valid insert
				properties.put("attrib1", "1");
				properties.put("attrib2", "2");
			} else {
				// invalid insert
				properties.put("attrib1", "3");
				properties.put("attrib2", "4");
			}
			try {
				createVertex(dbSession, type, properties);
				countInserts++;
				// System.out.println("Vertex '" + properties.get("name") + "' was created.");
			} catch (Exception e) {
				countRejects++;
				// System.out.println("Vertex '" + properties.get("name") + "' was not created: \n\t" + e.getMessage());
			} finally {
				properties.clear();
			}
		}
	}
}
