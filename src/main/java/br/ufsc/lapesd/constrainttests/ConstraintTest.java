package br.ufsc.lapesd.constrainttests;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.OrientDB;

import br.ufsc.lapesd.database.Connection;
import br.ufsc.lapesd.database.TestType;
import br.ufsc.lapesd.main.Utils;

/**
 * 
 * @author fabio
 *
 */
public abstract class ConstraintTest extends Connection {

	protected OrientDB orientDB;
	protected ODatabaseSession dbSession;
	protected StringBuilder sb;

	protected int countInserts = 0;
	protected int countRejects = 0;

	protected void createConnection() {
		orientDB = createConnection(CONNECTION_TYPE, CONNECTION_PATH, ROOT_USER, ROOT_PWD, DEFAULT_ORIENT_DB_CONFIG);
		dbSession = openODatabaseSession(orientDB, DATABASE_NAME, ADMIN_USER, ADMIN_PWD, DATABASE_TYPE);
	}

	public void closeConnection() {
		closeDatabaseSession(dbSession);
	}

	protected void dropDatabase() {
		orientDB = createConnection(CONNECTION_TYPE, CONNECTION_PATH, ROOT_USER, ROOT_PWD, DEFAULT_ORIENT_DB_CONFIG);
		if (DROP_DB)
			dropDatabase(orientDB, DATABASE_NAME);
		closeConnection(orientDB);
	}

	public void run(int execution) {
		sb = new StringBuilder();
	}
	
	protected void afterRun(){
		System.out.println("Implement afterRun");
	}

	protected abstract void createSchema();

	protected abstract void createConstraint();

	protected abstract void createEntities();

	protected abstract TestType getTestName();

	protected void deleteAllEntities(String type) {
		try {
			deleteVertex(dbSession, type, null);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	protected void updateStats(long startTime, long endTime) {
		long execTime = (endTime - startTime); // nanoseconds
		sb.append(getTestName().name());
		sb.append(",");
		sb.append(execTime / 1000000.0); // miliSeconds
		sb.append('\n');
	}

	public void printcsv(String csvPath) {
		if (sb != null)
			try (PrintWriter writer = new PrintWriter(new FileOutputStream(csvPath, true))) {
				writer.append(sb.toString());
			} catch (FileNotFoundException e) {
				System.out.println(e.getMessage());
			}
	}

	protected void beforeTest() {
		Utils.preheatCooldown();
	}

}
