package br.ufsc.lapesd.constrainttests;

import static br.ufsc.lapesd.database.TestType.UNIQUE;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import com.orientechnologies.orient.core.metadata.schema.OClass;

import br.ufsc.lapesd.database.TestType;

/**
 * 
 * @author fabio
 *
 */
public class UniqueConstraintTest extends ConstraintTest {

	public UniqueConstraintTest() {
		DATABASE_NAME = "Unique";
	}
	
	@Override
	protected TestType getTestName() {
		return UNIQUE;
	}
	
	public void run(int execution){
		super.run(execution);
		long startTime;
		long endTime;
		countInserts = 0;
		countRejects = 0;

		createConnection();
		
		if (CREATE_SCHEMA) createSchema();
		
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT) createEntities();
		endTime = System.nanoTime();

		if (DELETE_ALL_RECORDS) deleteAllEntities("Person");

		closeConnection();
		
		if (DROP_DB) dropDatabase();

		if(execution >= HEATING) // ignore the first 2 results for heating the JVM
			updateStats(startTime, endTime);
	}
	
	@Override
	protected void createSchema() {
		createNode("Person");
		if(CREATE_CONSTRAINT)
			createConstraint(); // it should run only once
		if(CREATE_UNIQUE_INDEX)
			createUniqueIndex(); // it should run only once
	}
	
	private void createNode(String name) {

	    OClass node = dbSession.getClass(name);
	    if (node == null)
	      node = dbSession.createVertexClass(name);
	    
	    if (node.getProperty("id") == null)
	    	node.createProperty("id", INTEGER);

	    if (node.getProperty("name") == null)
	      node.createProperty("name", STRING);

	}
	
	@Override
	protected void createConstraint() {
		String name = "uni";
		String target = "Person";
		String type = "UNIQUE";
		
		String sql = "CREATE CONSTRAINT " + name
				+ " ON " + target + " ( id ) " + type ;
		
		dbSession.execute("sql", sql, "");
	}
	
	protected void createUniqueIndex(){
		String name = "uniIndex";
		String target = "Person";
		String type = "UNIQUE";
		
		String sql = "CREATE INDEX " + name
				+ " ON " + target + " ( id ) " + type ;
		
		dbSession.execute("sql", sql, "");
		System.out.println(">>>>>>> Index Created!");
	}

	@Override
	protected void createEntities() {
		Map<String, String> properties = new HashMap<>();
		String type = "Person";

		for (int i = 0; i < NUM_INSERTS; i++) {
			if (i % MOD == 0) {
				// valid insert
				properties.put("id", "" + i);
			} else {
				// invalid insert
				properties.put("id", "" + (i - 1));
			}
			properties.put("name", "Person_" + i);
			try {
				createVertex(dbSession, type, properties);
				countInserts++;
				// System.out.println("Vertex '" + properties.get("name") + "' was created.");
			} catch (Exception e) {
				countRejects++;
				// System.out.println("Vertex '" + properties.get("name") + "' was not created: \n\t" + e.getMessage());
			} finally {
				properties.clear();
			}
		}
	}
}
