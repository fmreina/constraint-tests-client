package br.ufsc.lapesd.constrainttests;

import static br.ufsc.lapesd.database.TestType.CARDINALITY;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.Date;

import com.orientechnologies.orient.core.command.script.OCommandScript;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.sql.query.OBasicLegacyResultSet;

import br.ufsc.lapesd.database.TestType;

/**
 * 
 * @author fabio
 *
 */
public class CardinalityConstraintTest extends ConstraintTest {

	public CardinalityConstraintTest() {
		DATABASE_NAME = "Cardinality";
	}
	
	@Override
	protected TestType getTestName() {
		return CARDINALITY;
	}
	
	public void run(int execution){
		super.run(execution);
		long startTime;
		long endTime;

		createConnection();
		
		if (CREATE_SCHEMA) createSchema();
		
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT) createEntities();
		endTime = System.nanoTime();

		if (DELETE_ALL_RECORDS) {
			System.out.println("Deleting records "+ new Date());
			deleteAllEntities("Person");
		}

		closeConnection();
		
		if (DROP_DB) dropDatabase();

		if(execution >= HEATING) // ignore the first 2 results for heating the JVM
			updateStats(startTime, endTime);
	}
	
	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Owns");
//		createEdge("Lives_in");
		if(CREATE_CONSTRAINT)
			createConstraint(); // it should run only once
	}
	
	private void createNode(String name) {

	    OClass node = dbSession.getClass(name);
	    if (node == null)
	      node = dbSession.createVertexClass(name);
	    
	    if (node.getProperty("id") == null)
	    	node.createProperty("id", INTEGER);

	    if (node.getProperty("name") == null)
	      node.createProperty("name", STRING);

	    if (node.getProperty("attrib1") == null)
	      node.createProperty("attrib1", INTEGER);

	    if (node.getProperty("attrib2") == null)
		      node.createProperty("attrib2", INTEGER);

	}
	
	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
	    if (edge == null)
	      edge = dbSession.createEdgeClass(name);
	}
	
	@Override
	protected void createConstraint() {
		String name = "card";
		String target = "Person";
		String type = "CARDINALITY";
		String edgeType = "Owns";
		String lowerLimit = "N";
		String upperLimit = "3";
		String destiny = "Company";
		
		String sql = "CREATE CONSTRAINT " + name
				+ " ON " + target + " " + type + " " + edgeType 
				+ " " + lowerLimit + " .. " + upperLimit+ " TO " + destiny ;
		
		dbSession.execute("sql", sql, "");
	}

	@Override
	protected void createEntities() {
		for (int i = 0; i < NUM_INSERTS; i++) {
			try {
				if (i % MOD == 0) {
					// valid insert
					batchCreateValid(i);
				} else {
					// invalid insert
					batchCreateInvalid(i);
				}
				countInserts++;
			} catch (Exception e) {
				
				countRejects++;
			}
		}
	}
	
	private void batchCreateValid(int execNumber) {
		String cmd = "begin\n";
		cmd += "let person = create vertex Person set id= " + execNumber + ", name = 'Person_" + execNumber + "'\n";
		cmd += "let company1 = create vertex Company set id= " + execNumber + ", name = 'Company_1." + execNumber + "'\n";
		cmd += "let company2 = create vertex Company set id= " + execNumber + ", name = 'Company_2." + execNumber + "'\n";
		cmd += "let company3 = create vertex Company set id= " + execNumber + ", name = 'Company_3." + execNumber + "'\n";
		cmd += "let owns1 = create edge Owns from $person to $company1\n";
		cmd += "let owns2 = create edge Owns from $person to $company2\n";
		cmd += "let owns3 = create edge Owns from $person to $company3\n";

		// cmd += "let city = create vertex City set id= " + execNumber + ", name = 'City_." + execNumber + "'\n";
		// cmd += "let owns = create edge Lives_in from $person to $city\n";

		cmd += "commit retry 100\n";
		cmd += "return $owns1";

		OBasicLegacyResultSet edge = dbSession.command(new OCommandScript("sql", cmd)).execute();
		// edge.forEach(e -> System.out.println(e.toString()));
	}

	private void batchCreateInvalid(int execNumber) {
		String cmd = "begin\n";
		cmd += "let person = create vertex Person set id= " + execNumber + ", name = 'Person_" + execNumber + "'\n";
		cmd += "let company1 = create vertex Company set id= " + execNumber + ", name = 'Company_1." + execNumber + "'\n";
		cmd += "let company2 = create vertex Company set id= " + execNumber + ", name = 'Company_2." + execNumber + "'\n";
		cmd += "let company3 = create vertex Company set id= " + execNumber + ", name = 'Company_3." + execNumber + "'\n";
		cmd += "let owns1 = create edge Owns from $person to $company1\n";
		cmd += "let owns2 = create edge Owns from $person to $company2\n";
		cmd += "let owns3 = create edge Owns from $person to $company3\n";

		cmd += "let company4 = create vertex Company set id= " + execNumber + ", name = 'Company_4." + execNumber + "'\n";
		cmd += "let owns4 = create edge Owns from $person to $company4\n";

		// cmd += "let city = create vertex City set id= " + execNumber + ", name = 'City_." + execNumber + "'\n";
		// cmd += "let owns = create edge Lives_in from $person to $city\n";

		cmd += "commit retry 100\n";
		cmd += "return $owns1";

		OBasicLegacyResultSet edge = dbSession.command(new OCommandScript("sql", cmd)).execute();
		// edge.forEach(e -> System.out.println(e.toString()));
	}
}
