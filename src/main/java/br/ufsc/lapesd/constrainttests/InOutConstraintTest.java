package br.ufsc.lapesd.constrainttests;

import static br.ufsc.lapesd.database.TestType.INOUT;
import static com.orientechnologies.orient.core.metadata.schema.OType.INTEGER;
import static com.orientechnologies.orient.core.metadata.schema.OType.STRING;

import java.util.HashMap;
import java.util.Map;

import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.record.OVertex;

import br.ufsc.lapesd.database.TestType;

/**
 * 
 * @author fabio
 *
 */
public class InOutConstraintTest extends ConstraintTest {

	public InOutConstraintTest() {
		DATABASE_NAME = "InOut";
	}
	
	@Override
	protected TestType getTestName() {
		return INOUT;
	}
	
	public void run(int execution){
		super.run(execution);
		long startTime;
		long endTime;

		createConnection();
		
		if (CREATE_SCHEMA) createSchema();
		
		beforeTest();

		startTime = System.nanoTime();
		if (EXECUTE_INSERT) createEntities();
		endTime = System.nanoTime();

		if (DELETE_ALL_RECORDS) {
			deleteAllEntities("Person");
		}

		closeConnection();
		
		if (DROP_DB) dropDatabase();

		if(execution >= HEATING) // ignore the first 2 results for heating the JVM
			updateStats(startTime, endTime);
	}
	
	@Override
	protected void createSchema() {
		createNode("Person");
		createNode("Company");
		createNode("City");
		createEdge("Owns");
		if(CREATE_CONSTRAINT)
			createConstraint(); // it should run only once
	}
	
	private void createNode(String name) {

	    OClass node = dbSession.getClass(name);
	    if (node == null)
	      node = dbSession.createVertexClass(name);
	    
	    if (node.getProperty("id") == null)
	    	node.createProperty("id", INTEGER);

	    if (node.getProperty("name") == null)
	      node.createProperty("name", STRING);

	}
	
	private void createEdge(String name) {
		OClass edge = dbSession.getClass(name);
	    if (edge == null)
	      edge = dbSession.createEdgeClass(name);
	}
	
	@Override
	protected void createConstraint() {
		String name = "inout";
		String target = "Owns";
		String type = "IN_OUT_EDGE";
		String origin = "Person";
		String destiny = "Company";
		
		String sql = "CREATE CONSTRAINT " + name
				+ " ON " + target + " " + type 
				+ " FROM " + origin + " TO " + destiny;
		
		dbSession.execute("sql", sql, "");
	}

	@Override
	protected void createEntities() {
		Map<String, String> propertiesPerson = new HashMap<>();
		Map<String, String> propertiesCompany = new HashMap<>();
		Map<String, String> propertiesCity = new HashMap<>();
		String typePerson = "Person";
		String typeCompany = "Company";
		String typeCity = "City";
		String ownsType = "owns";

		for (int i = 0; i < NUM_INSERTS; i++) {
			propertiesPerson.put("id", "" + i);
			propertiesPerson.put("name", "Person_" + i);

			propertiesCompany.put("id", "" + i);
			propertiesCompany.put("name", "Company_" + i);
			
			propertiesCity.put("id", "" + i);
			propertiesCity.put("name", "City_" + i);
			
			try {
				OVertex person = createVertex(dbSession, typePerson, propertiesPerson);
				OVertex company = createVertex(dbSession, typeCompany, propertiesCompany);
				OVertex city = createVertex(dbSession, typeCity, propertiesCity);
				
				if (i % MOD == 0) {
					// valid insert
					createEdge(person, company, ownsType);
				} else {
					// invalid insert
					createEdge(person, city, ownsType);
				}
				
				countInserts++;
				// System.out.println("Vertex '" + properties.get("name") + "' was created.");
			} catch (Exception e) {
				
				countRejects++;
				// System.out.println("Vertex '" + properties.get("name") + "' was not created: \n\t" + e.getMessage());
			} finally {
				propertiesPerson.clear();
			}
		}
	}
}
