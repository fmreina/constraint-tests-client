package br.ufsc.lapesd.database;

import static com.orientechnologies.orient.core.db.ODatabaseType.PLOCAL;
import static com.orientechnologies.orient.core.db.OrientDBConfig.defaultConfig;

import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDBConfig;

/**
 * @author fabio
 */
public class Configuration {
	
//  public enum ExecMode {SCHEMA_MODIFIED, TEST_MODIFIED, SCHEMA_ORIGINAL, TEST_ORIGINAL, APPLICATION}
//  public enum TestType {
//	  CONDITIONAL, UNIQUE, INOUT, REQUIREDEDGE, CARDINALITY;
//  }

  // DATABASE CONFIG
  protected static String         CONNECTION_TYPE          = "remote";
  protected static String         CONNECTION_PATH          = "localhost";
  protected static OrientDBConfig DEFAULT_ORIENT_DB_CONFIG = defaultConfig();
  protected static String         ROOT_USER                = "root";
  protected static String         ROOT_PWD                 = "orientdb1234";
  protected static String         ADMIN_USER               = "admin";
  protected static String         ADMIN_PWD                = "admin";
  protected static ODatabaseType  DATABASE_TYPE            = PLOCAL;
  protected static String         DATABASE_NAME;

  // EXECUTION CONFIG
  protected static int     MOD                = 1;      // (1) all data correct (>1) part correct, part incorrect data

  public static int        	HEATING			   = 2; // to ignore the first X results
  public static int        	NUM_EXECUTIONS     = 0;	// number of executions of <NUM_INSERTS> inserts
  public static int			NUM_INSERTS        = 0;	// number of inserts on each execution
  public static boolean 	CREATE_SCHEMA      = true;
  public static boolean 	CREATE_CONSTRAINT  = false;
  public static boolean 	CREATE_UNIQUE_INDEX  = true;
  public static boolean 	EXECUTE_INSERT     = true;
  
  protected static boolean DELETE_ALL_RECORDS = false;
  protected static boolean DROP_DB            = false;

}
