package br.ufsc.lapesd.database;

import java.util.Map;
import java.util.Optional;

import com.orientechnologies.orient.core.db.ODatabaseSession;
import com.orientechnologies.orient.core.db.ODatabaseType;
import com.orientechnologies.orient.core.db.OrientDB;
import com.orientechnologies.orient.core.db.OrientDBConfig;
import com.orientechnologies.orient.core.record.OEdge;
import com.orientechnologies.orient.core.record.OElement;
import com.orientechnologies.orient.core.record.ORecord;
import com.orientechnologies.orient.core.record.OVertex;
import com.orientechnologies.orient.core.sql.executor.OResult;
import com.orientechnologies.orient.core.sql.executor.OResultSet;

/**
 * @author fabio
 */
public class Connection extends Configuration {

  protected OrientDB createConnection(String connectionType, String connectionPath, String user, String pwd,
      OrientDBConfig orientDBConfig) {
    return new OrientDB(connectionType + ":" + connectionPath, user, pwd, orientDBConfig);
  }

  protected void closeConnection(OrientDB orientDB) {
    orientDB.close();
  }

  protected ODatabaseSession openODatabaseSession(OrientDB orientDB, String dbName, String user, String pwd, ODatabaseType dbType) {
    orientDB.createIfNotExists(dbName, dbType);

    return orientDB.open(dbName, user, pwd);
  }

  protected void closeDatabaseSession(ODatabaseSession dbSession) {
    dbSession.close();
  }

  protected void dropDatabase(OrientDB orientDB, String dbName) {
    orientDB.drop(dbName);
  }

  protected void createSchema(ODatabaseSession dbSession) {
    // no schema by default
  }

  protected OResultSet deleteVertex(ODatabaseSession dbSession, String className, String condition) throws Exception {
    if (dbSession.getClass(className) != null) {
      if (condition != null) {
        return dbSession.execute("sql", "DELETE VERTEX ? WHERE ?;", className, condition);
      }
      return dbSession.execute("sql", "DELETE VERTEX ?;", className);
    }
    throw new Exception("Could not create the new vertex because the class " + className + "does not exists");
  }
  
  protected OResultSet deleteEdge(ODatabaseSession dbSession, String className, String condition) throws Exception {
	    if (dbSession.getClass(className) != null) {
	      if (condition != null) {
	        return dbSession.execute("sql", "DELETE EDGE ? WHERE ?;", className, condition);
	      }
	      return dbSession.execute("sql", "DELETE EDGE ?;", className);
	    }
	    throw new Exception("Could not Delete");
	  }
  
	protected void deleteRecord(ODatabaseSession dbSession, ORecord record) throws Exception {
		dbSession.delete(record);
	}
  
  protected OVertex createVertex(ODatabaseSession dbSession, String className, Map<String, String> properties) throws Exception {
	  if (dbSession.getClass(className) != null) {
		  OVertex result = dbSession.newVertex(className);
		  properties.keySet().forEach(p -> result.setProperty(p, properties.get(p)));
		  result.save();
		  return result;
	  }
	  throw new Exception("Could not create the new vertex because the class " + className + "does not exists");
  }

  protected OVertex createVertexIfNotExists(ODatabaseSession dbSession, String className, String property, String value,
      Map<String, String> properties) throws Exception {
    String query = String.format("SELECT FROM %s WHERE %s = ?", className, property);
    OResultSet rs = dbSession.query(query, value);

    return rs.hasNext() ? null : createVertex(dbSession, className, properties);
  }

  protected OEdge createEdge(OVertex out, OVertex in, String edgeType) {
    OEdge edge = out.addEdge(in, edgeType);
    edge.save();
    return edge;
  }
  
  protected OEdge createEdgeWithProperties(ODatabaseSession dbSession, OVertex out, OVertex in, String className, Map<String, String> properties) throws Exception {
	  if (dbSession.getClass(className) != null) {
//		  OEdge result = dbSession.newEdge(out, in, className);
		  OEdge result = out.addEdge(in, className);
		  for(String prop : properties.keySet())
			  result.setProperty(prop, properties.get(prop));
		  result.save();
		  return result;
	  }
	  throw new Exception("Could not create the new vertex because the class " + className + "does not exists");
  }
  
  protected OVertex selectFrom(ODatabaseSession dbSession, String className, String property, String value){
	  String query = String.format("SELECT FROM %s WHERE %s = ?", className, property);
	  OResultSet rs = dbSession.query(query, value);
	  
	  OVertex vertex = null;
      if (rs.hasNext()){
          OResult result = rs.next();
          OElement element = result.toElement();
          Optional<OVertex> optional = element.asVertex();
          if(optional.isPresent()) 
              vertex = optional.get();
      }
	  
	  return vertex;
  }
  
  protected OEdge selectEdgeFrom(ODatabaseSession dbSession, String className, String property, String value){
	  String query = String.format("SELECT FROM %s WHERE %s = ?", className, property);
	  OResultSet rs = dbSession.query(query, value);
	  
	  OEdge edge = null;
      if (rs.hasNext()){
          OResult result = rs.next();
          OElement element = result.toElement();
          Optional<OEdge> optional = element.asEdge();
          if(optional.isPresent()) 
              edge = optional.get();
      }
	  
	  return edge;
  }
}
