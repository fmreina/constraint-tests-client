package br.ufsc.lapesd.database;

public enum TestType {
	  CONDITIONAL, 
	  UNIQUE, 
	  INOUT, 
	  REQUIREDEDGE, 
	  CARDINALITY
}
